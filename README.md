# ODOO

<img src="https://www.anybox.fr/content/images/2019/06/odoo_logo--1--2.jpeg" width="300" height="200"/>


## INDEX

- [Introduction](#INTRODUCTION)
- [Prerequisites](#PREREQUISITES)
- [Install](#INSTALL)
- [License](#LICENSE)


## INTRODUCTION

one click app for :

- [caprover](https://caprover.com)


## PREREQUISITES

- Use [caprover](https://caprover.com)


## INSTALL

- Tutorial [one click app caprover](https://caprover.com/docs/one-click-apps.html#docsNav)


## LICENSE

[![GPLv3+](http://gplv3.fsf.org/gplv3-127x51.png)](https://gitlab.com/oda-alexandre/caprover_odoo/blob/master/LICENSE)
